Name:                qgnomeplatform
Version:             0.9.2
Release:             1
Summary:             The module provides Qt Theme aimed to accommodate Gnome settings
License:             LGPLv2+
URL:                 https://github.com/MartinBriza/QGnomePlatform
Source0:             https://github.com/MartinBriza/QGnomePlatform/archive/%{version}/QGnomePlatform-%{version}.tar.gz

BuildRequires:  make
BuildRequires:  pkgconfig(gio-2.0)
BuildRequires:  pkgconfig(udev)
BuildRequires:  pkgconfig(xkbcommon)
BuildRequires:  gtk3-devel
BuildRequires:  gsettings-desktop-schemas-devel
BuildRequires:  libinput-devel
BuildRequires:  libXrender-devel

%description
QGnomePlatform is a Qt Platform Theme aimed to accommodate as much of
GNOME settings as possibleand utilize them in Qt applications without
modifying them - making them fit into the environment as well as possible.

%package qt5
Summary:        Qt5 Platform Theme aimed to accommodate Gnome settings
BuildRequires:  qt5-qtbase-devel >= 5.15.2
BuildRequires:  qt5-qtbase-static >= 5.15.2
BuildRequires:  qt5-qtquickcontrols2-devel >= 5.15.2
BuildRequires:  qt5-qtwayland-devel >= 5.15.2
BuildRequires:  qt5-qtbase-private-devel >= 5.15.2
%{?_qt5:Requires: %{_qt5}%{?_isa} = %{_qt5_version}}

BuildRequires:  libadwaita-qt5-devel >= 1.4.2
Requires:       adwaita-qt5%{?_isa}

# Replace QGnomePlatform package with this as it was the Qt5 flavor
Obsoletes:      %{name} < 0.8.4-5
Provides:       %{name} = %{version}-%{release}
Provides:       %{name}%{?_isa} = %{version}-%{release}
Supplements:   (qt5-qtbase and gnome-shell)

%description qt5
QGnomePlatform is a Qt5 Platform Theme aimed to accommodate as much of
GNOME settings as possibleand utilize them in Qt applications without
modifying them - making them fit into the environment as well as possible.

%package qt6
Summary:        Qt6 Platform Theme aimed to accommodate Gnome settings
BuildRequires:  qt6-qtbase-devel >= 6.2.0
BuildRequires:  qt6-qtbase-static >= 6.2.0
BuildRequires:  qt6-qtquickcontrols2-devel >= 6.2.0
BuildRequires:  qt6-qtwayland-devel >= 6.2.0
BuildRequires:  qt6-qtbase-private-devel >= 6.2.0
BuildRequires:  libadwaita-qt6-devel >= 1.4.1

%{?_qt6:Requires: %{_qt6}%{?_isa} = %{_qt6_version}}
Requires:       adwaita-qt6%{?_isa}


%description qt6
QGnomePlatform is a Qt6 Platform Theme aimed to accommodate as much of
GNOME settings as possibleand utilize them in Qt applications without
modifying them - making them fit into the environment as well as possible.

%prep
%autosetup -p1 -n  QGnomePlatform-%{version}

%build
%global _vpath_builddir %{_target_platform}-qt5
%cmake -B "%{_vpath_builddir}" -DDECORATION_SHADOWS_SUPPORT=true
%__cmake --build "%{_vpath_builddir}"

%global _vpath_builddir %{_target_platform}-qt6
%cmake -B "%{_vpath_builddir}" -DUSE_QT6=true
%__cmake --build "%{_vpath_builddir}"

%install
%global _vpath_builddir %{_target_platform}-qt5
DESTDIR="%{buildroot}" %__cmake --install "%{_vpath_builddir}"

%global _vpath_builddir %{_target_platform}-qt6
DESTDIR="%{buildroot}" %__cmake --install "%{_vpath_builddir}"

%files qt5
%doc README.md
%license LICENSE
%{_datadir}/color-schemes/*.colors
%{_qt5_libdir}/libqgnomeplatform.so
%{_qt5_plugindir}/platformthemes/libqgnomeplatformtheme.so
%{_qt5_plugindir}/wayland-decoration-client/libqgnomeplatformdecoration.so

%files qt6
%doc README.md
%license LICENSE
%{_qt6_libdir}/libqgnomeplatform6.so
%{_qt6_plugindir}/platformthemes/libqgnomeplatformtheme.so
%{_qt6_plugindir}/wayland-decoration-client/libqgnomeplatformdecoration.so
%exclude %{_datadir}/color-schemes/*.colors

%changelog
* Tue Nov 21 2023 lwg <liweiganga@uniontech.com> - 0.9.2-1
- update to version 0.9.2

* Sat Jul 31 2021 chenyanpanHW <chenyanpan@huawei.com> - 0.5-9
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Mon May 31 2021 huanghaitao <huanghaitao8@huawei.com> - 0.5-8
- Completing build dependencies to fix git commands missing error

* Tue Apr 21 2020 Jeffery.Gao <gaojianxing@huawei.com> - 0.5-7
- Package init
